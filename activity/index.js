/*

	Activity:

	1. Update and Debug the following codes to ES6.
		Use template literals,
		Use array/object destructuring,
		Use arrow functions
	
	2. Create a class constructor called Movie able to receive 5 arguments
		-Using the this keyword assign properties:
			title, //string
			director, //string
			producer, //string
			year, //num
			cast, //array 
			-assign the parameters as values to each property.

	Create 2 new objects using our class constructor.

	Log the 2 new Movie objects in the console.


	Pushing Instructions:

	Go to Gitlab:
		-in your zuitt-projects folder and access b123 folder.
		-inside your b152 folder create a new folder/subgroup: s19
		-inside s19, create a new project/repo called activity
		-untick the readme option
		-copy the git url from the clone button of your activity repo.

	Go to Gitbash:
		-go to your b123/s19 folder and access activity folder
		-initialize activity folder as a local repo: git init
		-connect your local repo to our online repo: git remote add origin <gitURLOfOnlineRepo>
		-add your updates to be committed: git add .
		-commit your changes to be pushed: git commit -m "includes es6 updates activity"
		-push your updates to your online repo: git push origin master

	Go to Boodle:
		-copy the url of the home page for your s19/activity repo (URL on browser not the URL from clone button) and link it to boodle:

		WD078-19 | Javascript - ES6 Updates


*/

//Solution: 

/*Debug*/
let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101", "Social Sciences 201"]
}

	let student2 = {
		name: "Steve Austin",
		birthday: "June 15, 2001",
		age: 20,
		isEnrolled: true,
		classes: ["Philosphy 401", "Natural Sciences 402"]
	}



//Debug and Update to Es6


	const introduce = (student) =>{

		let {name,age,classes} = student;

		console.log(`Hi! I'm ${name}. I am ${age} years old.`)
		console.log(`I study the following courses: ${classes}`)

	}



introduce(student1);
introduce(student2);



const getCube = num => {

	console.log(num**3)

}

getCube(10);



let numArr = [15,16,32,21,21,2]

let [num1,num2,num3] = numArr;

console.log(num1);
console.log(num2);
console.log(num3);



numArr.forEach((num) => console.log(num));


let numsSquared = numArr.map((num) => num ** 2);



console.log(numsSquared)


/*2. Class Constructor*/



class Movie {
	constructor(title,director,producer,year,cast){
		this.title = title;
		this.director = director;
		this.producer = producer;
		this.year = year;
		this.cast = cast;

	}

};

let Movie1 = new Movie ("Matrix 4","Lana Wachowski","Grant Hill",2022,["Keanu Reeves","Carrie-Anne Moss"]);
console.log(Movie1)
let Movie2 = new Movie ("Spider-Man: No Way Home", "Jon Watts", "Kevin Feige",2021, ["Tom Holland", "Zendaya"]);
console.log(Movie2)



















