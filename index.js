/*

	ES6 Updates

	Exponents

	Match.pow() to get the result of a number raised to a given exponent.
	Match.pow(base,exponent)
	*/

	//let fivePowerof3 = Math.pow(5,3);
	//console.log(fivePowerof3);

	//Exponent Operators ** = allow to gert the result of a number raised to a given
	//exponent

	let fivePowerof3 = 5**3;
	console.log(fivePowerof3);//125

	let fivePowerOf2 = 5**2;
	console.log(fivePowerOf2);
	let fivePowerOf4 = 5**4;
	console.log(fivePowerOf4);
	let twoPowerOf2 = 2**2;
	console.log(twoPowerOf2)

function squareRootChecker(num){
	return num **.5;
}

let squareRootOf4 = squareRootChecker(4);
console.log(squareRootOf4);

/*Template Literals

An ES6 update to creating strings. Represented by backticks (``).
*/
let sampleString1 =`Charlie`;
let sampleString2 = `Joy`;
console.log(sampleString1);
console.log(sampleString2);


//let combinedString = sampleString1 + sampleString2

let combinedString = `$(sampleString1) $(sampleString2)`;

//create a single string to comine our variables. This is with 
//the use of template literals(``) and by embedding JS experessions and variables
//in the string using ${} or placeholders.






console.log(combinedString);
//template literals with JS expresssions

let num1 = 15;
let num2 = 3;

//what if we want to say a sentence in the console like this:
//'The result of 15 plus 3 id 18'


let sentence = `The result of ${num1} plus ${num2} is ${num1+num2}`
console.log(sentence);



let resultOf15ToPowerOf2 = 15**2;

let sentence2 = `The result of ${15} to the power of ${2} is ${resultOf15ToPowerOf2}`;
console.log(sentence2);

let resultOf3Times6 = 3*6;

let sentence3 = `The result of ${3} times ${6} is ${resultOf3Times6}`;
console.log(sentence3);

let array = ["kobe","Lebron","Jordan"]
/*let goat1 = array[0];

console.log(goat1);

let goat2 = array[1];
let goat3 = array[2];
console.log(goat2,goat3);

*/

let [goat1,goat2,goat3] = array;
console.log(goat1);
console.log(goat2);
console.log(goat3);


let array2 = ["Curry","Lillard","Paul","Irving"];

let [pg1,pg2,pg3,pg4] = array2;
console.log(pg1);
console.log(pg2);
console.log(pg3);
console.log(pg4);


//In arrays, the order of items/elements is important.
//you can skip an item by adding another separator(,) and by 
//not providing a variable to save the item we want to skip.

let array3 = ["Jokic","Embiid","Anthony-Towns","Gober"];
let[center1,,center3] = array3;
console.log(center1);
console.log(center3);


let array4 = ["Draco Malfoy","Hermione Granger","Harry Potter", "Ron Weasley","Professor Snape"]


let [,gryffindor1,gryffindor2,gryffindor3] = array4;
console.log(gryffindor1);
console.log(gryffindor2);
console.log(gryffindor3);

/* Destructuring call allow us to destructure an array and save items in variables.
also into constants.

let/const [variable1,variable2] = array;

*/

const [slytherin1,,,,slytherin2] = array4;
console.log(slytherin1);
console.log(slytherin2);

gryffindor1 = "Emma Watson";
console.log(gryffindor1);

//slytherin1 = "Tom Felton";
//console.log(slytherin1);

/* Object Destructuring
It will allow us to destructure an object by savind/add the values of an 
object's property into respective variables.*/

let pokemon = {
	name: "Blastoise",
	level: 40,
	health: 80
}

let sentence4 = `The pokemon's name is ${pokemon.name}.`;
let sentence5 = `It is a level ${pokemon.level} pokemon.`;
let sentence6 = `It has at least ${pokemon.health} health points.`;


console.log(sentence4);
console.log(sentence5);
console.log(sentence6);

//we can save the values of an object's properties into variables.
//By Object Destructuring:

//Array Destructuring order was important and the name of the variable we 
//save our items in is not important.

//Object destructuring, order is not important however the name of the variables
//should match the propertiesof the object.


let {health,name,level} = pokemon;
console.log(health);
console.log(name);
console.log(level);
//console.log(trainer);//undefined because there is no trainer property in the object


let person = {
	name: "Paul Phoenix",
	age: 31,
	birthday: "January 12, 1991"
}


function greet({name,age,birthday}){

	//destructure your object in the function
	//let {name,age,birthday} = object;


	console.log(`Hi! My name is ${name}`);

	console.log(`I am ${age} years old.`);

	console.log(`My birthday is on ${birthday}`);


}

greet(person);

let actors = ["Tom Hanks", "Leonardo Dicaprio","Anthony Hopkins","Ryan Reynolds"];

let director = {
	name: "Alfred Hitchcock",
	birthday: "August 13, 1889",
	isActive: false,
	movies: ["This Birds","Psycho","North by Northwest"]
};

let [actor1,actor2,actor3] = actors;


function displayDirector(person){
	
	let [Name,birthday,movies] = person;

	console.log(`The Director's name is ${director.name}`);
	console.log(`He was born on ${director.birthday}`);
	console.log(`His Movies include:`);
	console.log(director.movies);
}

console.log(actor1);
console.log(actor2);
console.log(actor3);

//displayDirector(director);


/* Arrow Functions

	Arrow Functions are an alternative way of writing/declaring functions.
	However, there are significant differences between our regular/traditional
	function and arrow function.
*/

	//const hello == () => {
	//	console.log(`Hello from Arrow!`)
	//}

	//hello();

	//function greet(personParams){

	//	console.log(`Hi, ${personParams.name}!`);
	
	//};


	//const hi = (personParams) => {
	//	console.log(`Hi, ${personParams.name}!`);
	//}

	//hi(person);



//anonymous functions in array methods

/* It allows us to iterate/loop over all items in an array.
An anonymous function is added so we can perform tasks per item inthe array
This anonymous function recieves the currewnt item being iterated/loop

actors.forEach(function(actor){
	console.log(actor);
})

*/
//traditional functions cannot work without curly braces.
//arrow functions can ommit curly braces but have to be in one line


actors.forEach(actor=>console.log(actor))

let numberArr = [1,2,3,4,5];

//let multipliedBy5 = numberArr.map(function(number){

//	return number*5

//})

//implicit return
let multipliedBy5 = numberArr.map(number=>number*5)

//when arrow function has {} it will need the return keyword to return a value

console.log(multipliedBy5);

/*Implict return for Arrow Functions*/

//function addNum(num1,num2){

//	return num1+num2;

//};

//let sum1 = addNum(5,10);
//console.log(sum1);


const subtractNum = (num1,num2) => num1 - num2;
let difference1 = subtractNum(45,15);
console.log(difference1)


const addNum = (num1,num2) => num1 + num2;

let sumExample = addNum(50,10);
console.log(sumExample)


let protagonist = {
	name: "Cloud Strife",
	occupation: "SOLDIER",
	greet: function(){
		console.log(this);
		console.log(`Hi! I am ${this.name}`);
	},
	introduceJob: () => {
		//this refers to the global window object or the whole page.
		console.log(this)
		console.log(`I work as a ${this.occupation}`)
	}
}

protagonist.greet();//this refers to parent object.
protagonist.introduceJob();//results to undefined cuz this in an arrow function 
//refers to the global window object.

//don't use 'this' in an arrow method

/* Class-Base Objects blueprints
 In js, classes are templates/blueprints to creeating objects.
 WE can ccreate objects out of the us of Classes.
 Before the introduction of Classes in JS, we mimic this behavior to create 
 objects out mof templates with the use of constructor function


 */

//Constructor function
	 function Pokemon(name,type,level){

	 		this.name = name;
	 		this.type = type;
	 		this.level = level;
	 }

	 let pokemon1 = new Pokemon("Sandshrew","Ground",15);
	 console.log(pokemon1);


//Es6 class creation
	//we have a more distinctive way of creating classes instead of just 
	//our constructor functions

	class Car {
		constructor(brand,name,year){
			this.brand = brand;
			this.name = name;
			this.year = year;
		}
	}

	let car1 = new Car("Toyota","Vios","2002");
	console.log(car1);














